##¿Qué es Ionic?
Es un framework que permite crear app hibridas donde se reutiliza el código para diferentes plataformas del sistemas operativos soportados por 
dispositivos móviles.

##Paquetes Necesaruios para la creación de una app en Ionic:

Editor de texto: Sublime, notepad++, VSCode o cualquier otro editor
Aqui algunas url de descarga:

https://www.sublimetext.com/
https://notepad-plus-plus.org/download/v7.5.1.html
https://code.visualstudio.com/

Ionic
Cordova
NodeJS
Angular

##Instalación de Ionic y Cordova

Abrir una terminal e ingresar el siguiente comando dependiendo del Sistema Operativo Utilizado

Windows: sudo npm install -g cordova ionic
Linux: sudo apt-get install nodejs
sudo apt-get install npm

##Creación de una app en ionic

En la terminal ejecutar el comando: ionic start "nombre_app" blank
Nota: blank permite crear una app a partir de una plantilla vacía

Una vez creado el proyecto, ya podemos empezar a desarrollar sobre él.

##Documentación IONIC

Toda la documentación sobre los componentes de ionic puede encontrarla en: 
https://ionicframework.com/docs/


