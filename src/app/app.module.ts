import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import {HttpModule} from '@angular/http';
import { MyApp } from './app.component';
import {HttpService} from '../services/http-service';
import {InicioPage} from '../pages/inicio/inicio'
import {RegistroPage} from '../pages/registro/registro'
import {MenuclientePage} from '../pages/menucliente/menucliente'
import{SolicitarservicioPage} from '../pages/solicitarservicio/solicitarservicio'
import{MenuoperarioPage} from '../pages/menuoperario/menuoperario'
import {LocalStorageModule} from 'angular-2-local-storage';
import{HistorialserviciosPage} from '../pages/historialservicios/historialservicios'
import{ServiciosoperarioPage} from '../pages/serviciosoperario/serviciosoperario'
import{OrigendestinoPage} from  '../pages/origendestino/origendestino'
import{ArmadooperaciondesarmaPage} from '../pages/armadooperaciondesarma/armadooperaciondesarma'
import{CancelarservicioPage} from '../pages/cancelarservicio/cancelarservicio'
import{EncuestaPage} from '../pages/encuesta/encuesta'
import{ServiciosculminadosPage} from '../pages/serviciosculminados/serviciosculminados'
import{TicektsoperadorPage} from '../pages/ticektsoperador/ticektsoperador'
import{ArmadodesarmaoperadorPage} from '../pages/armadodesarmaoperador/armadodesarmaoperador'
import{OpcionesmenuclientePage} from '../pages/opcionesmenucliente/opcionesmenucliente';
import{TicketsPage} from "../pages/tickets/tickets";
import{MenuservactivosPage} from "../pages/menuservactivos/menuservactivos";
import{SolosalirPage} from "../pages/solosalir/solosalir";
import{MenumapaoperadorPage} from "../pages/menumapaoperador/menumapaoperador";
import{MenuppaloperadorPage} from "../pages/menuppaloperador/menuppaloperador";
import{MenuppalsaliroperadorPage} from "../pages/menuppalsaliroperador/menuppalsaliroperador";


import { Geolocation } from '@ionic-native/geolocation';
import { Push } from '@ionic-native/push';
import {VARIALES} from '../services/mock-vars';
import{Camera} from '@ionic-native/camera'; 
@NgModule({
  declarations: [
    MyApp,
    InicioPage,
    RegistroPage,
    MenuclientePage,
    SolicitarservicioPage,
    HistorialserviciosPage,
    MenuoperarioPage,
    ServiciosoperarioPage,
    TicketsPage,
    OrigendestinoPage,
    ArmadooperaciondesarmaPage,
    CancelarservicioPage,
    EncuestaPage,
    ServiciosculminadosPage,
    TicektsoperadorPage,
    ArmadodesarmaoperadorPage,
    OpcionesmenuclientePage,
    MenuservactivosPage,
    SolosalirPage,
    MenumapaoperadorPage,
    MenuppaloperadorPage,
    MenuppalsaliroperadorPage
    
    
    
  ],
  imports: [
    HttpModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    LocalStorageModule.withConfig({
            prefix: 'my-app',
            storageType: 'localStorage'
        })

    
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    InicioPage,
    RegistroPage,
    MenuclientePage,
    SolicitarservicioPage,
    HistorialserviciosPage,
    MenuoperarioPage,
    ServiciosoperarioPage,
    TicketsPage,
    OrigendestinoPage,
    ArmadooperaciondesarmaPage,
    CancelarservicioPage,
    EncuestaPage,
    ServiciosculminadosPage,
    TicektsoperadorPage,
    ArmadodesarmaoperadorPage,
    OpcionesmenuclientePage,
    MenuservactivosPage,
    SolosalirPage,
    MenumapaoperadorPage,
    MenuppaloperadorPage,
    MenuppalsaliroperadorPage
    

  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpService,
    Geolocation,
    VARIALES,
    Push,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
