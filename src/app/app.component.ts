import { Component } from '@angular/core';
import { Platform, AlertController  } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import{InicioPage} from '../pages/inicio/inicio';
import { Push, PushObject, PushOptions } from '@ionic-native/push';
import {LocalStorageService} from 'angular-2-local-storage';



@Component({
  templateUrl: 'app.html'
})
export class MyApp { 
  rootPage:any = InicioPage; //pagina por defecto. Aqui debería ir la del login
  public pages = [
    {
    title: 'Inicio',
    icon: 'ios-home-outline',
    count: 0,
    component: InicioPage
    },
    
    {
    title: 'Categorias',
    icon: 'ios-list-box-outline',
    count: 0,
    component: InicioPage
    },     
    ]
  constructor(private localStorageService: LocalStorageService,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public push: Push , public alertCtrl: AlertController) {
    platform.ready().then(() => { //ya se está en una plataforma ios, android, entre otras
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
      this.pushsetup();
      
       

    });
  }
  pushsetup() {
    const options: PushOptions = {
     android: {
        // senderID: 'AIzaSyAdthccJCR9Jfz_YG97Ptoc10eaOsFwKec'
     },
     ios: {
         alert: 'true',
         badge: true,
         sound: 'false'
     },
     windows: {}
  };
 
  const pushObject: PushObject = this.push.init(options);
 
  pushObject.on('notification').subscribe((notification: any) => {
    if (notification.additionalData.foreground) {
      let youralert = this.alertCtrl.create({
        title: notification.title,
        message: notification.message
      });
      youralert.present();
    }
  });
 
  pushObject.on('registration').subscribe((registration: any) => {
    this.localStorageService.set('token_registration', registration.registrationId);
  });  

 
  pushObject.on('error').subscribe(error => alert('Error with Push plugin' + error));
  }
}

