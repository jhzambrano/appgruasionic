import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import {InicioPage} from "../inicio/inicio"
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MenuclientePage} from "../menucliente/menucliente";
import {Http, RequestOptions, RequestMethod, Headers} from '@angular/http';
import {HttpService} from '../../services/http-service';
import {VARIALES} from '../../services/mock-vars';
import 'rxjs/add/operator/map';


@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  slideOneForm: FormGroup; 
  name :String;
  lastname: String;
  business :String;    
  phone :String;
  address :String;
  user:String;
  email :String;
  password :String;
  password_repeat :String;
  error:any;
  response_:any = [];
  response_d:any = [];

  constructor(private variables:VARIALES, private httpService:HttpService, private http:Http,public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
   this.slideOneForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required ])],
        password: ['', Validators.compose([Validators.required])],
        name: ['', Validators.compose([Validators.required])],
        lastname: ['', Validators.compose([Validators.required])],
        business: ['', Validators.compose([Validators.required])],
        phone: ['', Validators.compose([Validators.required])],
        address: ['', Validators.compose([Validators.required])],
        password_repeat: ['', Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {
  //  console.log('ionViewDidLoad RegistroPage');
  }
  registro()
  {
    if(this.slideOneForm.valid && this.password_repeat == this.password){


      this.response_ = this.httpService.post(this.variables.urlbase+"api/register",
        {
          email: this.email,  
          password: this.password, 
          password_confirmation: this.password_repeat,
          name: this.name,           
          lastname:this.lastname,
          business: this.business, 
          telephone: this.phone, 
          address: this.address,  
          id_roll_user:1,
          esionic:1 
        } );

        this.response_d = JSON.parse(this.response_);
   
        if(this.response_d.status == "200"){
              if(this.response_d.rsp.error==1) //paso
              {

                let alert = this.alertCtrl.create({
                  title: 'Registro de usuario',
                  subTitle: 'El registro se ha creado en la Base de Datos',
                  buttons: ['OK']
                });
                alert.present()
                this.navCtrl.push(InicioPage);
              }
              else
              {
                let alert = this.alertCtrl.create({
                  title: 'Registro de usuario',
                  subTitle: 'Hubo un fallo en los datos suministrados',
                  buttons: ['OK']
                });
                alert.present();
              }

             }

    
      
    }else{
      let alert = this.alertCtrl.create({
                  title: 'Registro de usuario',
                  subTitle: 'Hubo un fallo en los datos suministrados',
                  buttons: ['OK']
                });
                alert.present()
    }
    
  }
  menucliente(){
    this.navCtrl.push(MenuclientePage);
  }

}
