import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { InicioPage } from "../inicio/inicio"
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MenuclientePage } from "../menucliente/menucliente";
import { Http, RequestOptions, RequestMethod, Headers } from '@angular/http';
import { HttpService } from '../../services/http-service';
import { VARIALES } from '../../services/mock-vars';
import 'rxjs/add/operator/map';
import { LocalStorageService } from 'angular-2-local-storage';
import { FormControl } from '@angular/forms';


import { AbstractControl, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

@IonicPage()
@Component({
  selector: 'page-registro',
  templateUrl: 'registro.html',
})
export class RegistroPage {

  slideOneForm: FormGroup;
  name: String;
  lastname: String;
  business: String;
  phone: String;
  address: String;
  user: String;
  email: String;
  password: String;
  password_repeat: String;
  typeuser: number;
  error: any;
  response_: any = [];
  response_d: any = [];
  existeemail: any = [];

  constructor(private localStorageService: LocalStorageService, private variables: VARIALES, private httpService: HttpService, private http: Http, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
    this.slideOneForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required,Validators.minLength(7)])],
      name: ['', Validators.compose([Validators.required])],
      lastname: ['', Validators.compose([Validators.required])],
      business: ['', Validators.compose([Validators.required])],
      phone: ['', Validators.compose([Validators.required])],
      address: ['', Validators.compose([Validators.required])],
      password_repeat: ['', Validators.compose([Validators.required,Validators.minLength(7), this.matchOtherValidator('password')])]
    });

  }
  matchOtherValidator(otherControlName: string): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const otherControl: AbstractControl = control.root.get(otherControlName);

      if (otherControl) {
        const subscription: Subscription = otherControl
          .valueChanges
          .subscribe(() => {
            control.updateValueAndValidity();
            subscription.unsubscribe();
          });
      }

      return (otherControl && control.value !== otherControl.value) ? { match: true } : null;
    };
  }

  registro() {
    if (this.slideOneForm.valid && this.password_repeat == this.password) {

      //se verifica si el correo ya está registrado
      this.response_ = this.httpService.post2(this.variables.urlbase + "api/verificaremail",
        {
          email: this.email

        });
      this.response_.map(res => res)
        .subscribe(
        res => {
          this.existeemail = res.json();
        }
        );        
      if (this.existeemail.existe == 'no' && this.password==this.password_repeat) //ese correo no está registrado
      {
        this.response_ = this.httpService.post(this.variables.urlbase + "api/register",
          {
            email: this.email,
            password: this.password,
            password_confirmation: this.password_repeat,
            name: this.name,
            lastname: this.lastname,
            business: this.business,
            telephone: this.phone,
            address: this.address,
            id_roll_user: 1,
            esionic: 1,
            typeuser: 1
          });

        this.response_d = JSON.parse(this.response_);

        if (this.response_d.status == "200") {
          if (this.response_d.rsp.error == 1) //paso
          {

            let alert = this.alertCtrl.create({
              title: 'Registro de usuario',
              subTitle: 'El registro se ha creado en la Base de Datos',
              buttons: ['OK']
            });
            alert.present()
            this.navCtrl.push(InicioPage);
          }
          else {
            let alert = this.alertCtrl.create({
              title: 'Registro de usuario',
              subTitle: 'Hubo un fallo en los datos suministrados. Verifique la Dirección de Correo Electrónico y que las contraseñas coincidan',
              buttons: ['OK']
            });
            alert.present();
          }

        }
      }
      else
      {
        if(this.existeemail.existe == 'si')
        {
          let alert = this.alertCtrl.create({
            title: 'Registro de usuario',
            subTitle: 'El Correo ingresado ya está Registrado!!! ',
            buttons: ['OK']
          });
          alert.present()
        }
        
      }



    } else {
      let alert = this.alertCtrl.create({
        title: 'Registro de usuario',
        subTitle: 'Hubo un fallo en los datos suministrados. Verifique la Dirección de Correo Electrónico y que las contraseñas coincidan',
        buttons: ['OK']
      });
      alert.present()
    }

  }
  menucliente() {
    //se verifica si existe la variable de session de id_user
    
    if (this.localStorageService.get('id_user') != "")
      this.navCtrl.push(MenuclientePage);
    else {
      //alert para indicar al usuario que no está logueado...
      let alert = this.alertCtrl.create({
        title: 'No existe usuario logueado',
        subTitle: 'Por favor inicie sesión para acceder al menu',
        buttons: ['OK']
      });
      alert.present();
      this.navCtrl.push(InicioPage);
    }

  }
  login() {
    this.navCtrl.push(InicioPage);
  }

}
