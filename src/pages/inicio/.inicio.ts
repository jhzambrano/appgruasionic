import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Platform } from 'ionic-angular';
import {RegistroPage} from "../registro/registro";
import {MenuclientePage} from "../menucliente/menucliente";
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from 'angular-2-local-storage';
import {HttpService} from '../../services/http-service';
import {VARIALES} from '../../services/mock-vars';
import {Http, RequestOptions, Headers} from '@angular/http'; 
import 'rxjs/add/operator/map';   
@IonicPage()


@Component({
  selector: 'page-inicio',
  templateUrl: 'inicio.html',
  
})

export class InicioPage {
  email :any;
  password :any;
  private clientId:string = this.variables.clientId;
	private clientSecret:string =  this.variables.clientSecret; 
	private grantType:string = this.variables.grantType;   
  response:any = [];
  response2:any = [];
  response_:any = [];
  response_d:any = [];
  response_d2:any = [];
  response2_:any=[];
  responseData:any = [];
  responseData2:any = [];
  status:any;
  var:any = [];
  slideOneForm: FormGroup; 
  
  constructor(private variables:VARIALES, private httpService:HttpService, private localStorageService: LocalStorageService, private http:Http, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams, public alertCtrl:AlertController ) {
    
this.slideOneForm = formBuilder.group({
        email: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(8), Validators.maxLength(120)])],
        password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])]
    });
  }

  ionViewDidLoad() {
    console.log('Inicio pagina cargada');                 
  }
  iniciosesion(){ 
    if(!this.slideOneForm.valid){
        let alert = this.alertCtrl.create({
        title: "Error",
        subTitle: 'Debe ingresar un email valido y una contraseña de al menos 6 caracteres',
        buttons: ['OK']
        });
        alert.present();
    }else{
 

  //----------------------------------------------------------------      
  this.response_ = this.httpService.post(this.variables.urlbase+"oauth/token",
        {
          client_id: this.clientId,
          client_secret: this.clientSecret,
          grant_type: this.grantType,
          username: this.email,          
          password: this.password
        } );

        this.response_d = JSON.parse(this.response_);
       this.response = this.response_d.rsp;      
        if(this.response_d.status == "200"){
          this.responseData  = this.response;
               this.localStorageService.set('token', this.responseData.access_token);
               //se obtiene el id del usuario                           
               this.response_ = this.httpService.post2(this.variables.urlbase+"api/obtenerid",
               {
                  username: this.email
               });
               this.response_.map(res=>res)
               .subscribe(
                 res=>{
                    this.status=res.status;
                    this.var=res.json();                    
                    this.localStorageService.set('id_user',this.var[0]['id_user']);
                 },
                 err =>{
                   this.status=err.status
                   console.log(err);
                 }
               );
               
               let alert = this.alertCtrl.create({
                  title: 'Inicio de Sesión Exitoso!!!',
                  subTitle: 'Los Datos Han sido verificados en la BD',
                  buttons: ['OK']
                });
                alert.present();
               this.navCtrl.push(MenuclientePage);

                this.httpService.post(this.variables.urlbase+"api/tokenregister",
                {
                  token: this.localStorageService.get('token_registration'),
                  id:  this.localStorageService.get('id_user')
                } );


             } 
        if (this.response_d.status == 401) {
                 let alert = this.alertCtrl.create({
                  title: 'Inicio de Sesión fallido!!!',
                  subTitle: 'Los Datos No Han sido verificados en la BD',
                  buttons: ['OK']
                });
                alert.present();
                }     
    }

  }
  registro(){
    this.navCtrl.push(RegistroPage);
  } 

  notif2(){  
  
            let headers = new Headers();
          headers.append('Content-Type', 'application/json'); 
          headers.append('Authorization', 'key=AAAAMP3zBhM:APA91bHMs6Eil_O4gR4iSRqVg6m1oZ1Sxj5BmAW3bVScSc7iVIM5mxi4HUiS4zwNNtlOPDuK9TkHNuUJAkLlc5Fiz6xBiCSnuxQBU7UQNI7NlkI-y_11wj_wzSeKqSirzUQKgxvmFApa');   
          let options = new RequestOptions({ headers: headers });

          this.http.post("https://fcm.googleapis.com/fcm/send", 
          JSON.stringify({
            notification:{
              "title": "Your Title",
              "text": "Your Text",              
           },
            "to" :  "do7t1u9KirI:APA91bEZTiJfRSR8E3NMAfRDK2NMPmokkG_Ccodlpzr4vFeCyTx9t8jCntaAcNUnIv0zobiKZN7TB6xCgoq5A2MaHVQZ_Q7gxatnvvXUCikdm2Dj-bXwKOtAh5t-0ahOhl4Q-h1iF3dF"  
        }), 
       options 
        ) 
        .subscribe(
          data => {  
             this.response = data;
             this.responseData  = this.response.json();
             
             if(this.response.ok &&  this.response.status == "200"){
                console.log(data);
             } 
          },			
          err => { err=err
            if (err.status == 401) {
                  
                }
           console.log(err);
          }
        );   
  }
 
   

}
