import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TicektsoperadorPage } from './ticektsoperador';

@NgModule({
  declarations: [
    TicektsoperadorPage,
  ],
  imports: [
    IonicPageModule.forChild(TicektsoperadorPage),
  ],
})
export class TicektsoperadorPageModule {}
