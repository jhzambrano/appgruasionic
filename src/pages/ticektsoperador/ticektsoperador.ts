import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController,ViewController,MenuController } from 'ionic-angular';
import {LocalStorageService} from 'angular-2-local-storage';
import{OrigendestinoPage} from '../origendestino/origendestino';
import{ArmadooperaciondesarmaPage} from '../armadooperaciondesarma/armadooperaciondesarma';
import{InicioPage} from '../inicio/inicio'
import{MenuoperarioPage} from '../menuoperario/menuoperario'
import{ServiciosoperarioPage} from '../serviciosoperario/serviciosoperario';
import{MenuclientePage} from '../menucliente/menucliente';
import{HistorialserviciosPage} from '../historialservicios/historialservicios';
import{ArmadodesarmaoperadorPage} from '../armadodesarmaoperador/armadodesarmaoperador';
import{OpcionesmenuclientePage} from '../opcionesmenucliente/opcionesmenucliente';
import{MenumapaoperadorPage} from '../menumapaoperador/menumapaoperador';

import{EncuestaPage} from "../encuesta/encuesta";
import { ModalController } from 'ionic-angular';
import { HttpService } from '../../services/http-service';
import { VARIALES } from '../../services/mock-vars';
import { PopoverController } from 'ionic-angular';
import{MenuppalsaliroperadorPage} from '../menuppalsaliroperador/menuppalsaliroperador';

/**
 * Generated class for the TicketsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-ticektsoperador',
  templateUrl: 'ticektsoperador.html',
})
export class TicektsoperadorPage {
  items2:any=[];
  nombre_usuario:any;
  nivelencuesta:number;
  fecha: any;
  response_: any=[];
  id_servicio:number;
  status: any=[];
  items: any=[];  
  nombre: any;
  esoperador: any;
  public items3: any=[];
  public armado2: any=ArmadooperaciondesarmaPage;
  constructor(public navCtrl: NavController, public navParams: NavParams,private localStorageService: LocalStorageService,private alertCtrl: AlertController,public modalCtrl: ModalController, private view: ViewController,private variables: VARIALES,private httpService: HttpService,public menuCtrl: MenuController,public popoverCtrl: PopoverController) {    
    this.obtenerdatos();
  }
  openModal(){
    //this.navCtrl.push(MenumapaoperadorPage)
    let popover = this.popoverCtrl.create(MenuppalsaliroperadorPage);
    popover.present({      
    });
    
  }
obtenerdatos()
{      
  //se obtiene la data enviada de serviciosoperario.ts
  this.nombre_usuario=this.localStorageService.get('nameuser');
  this.items2=this.navParams.data;  
  
  //se crea la variable de session con el id de servicio  
  this.localStorageService.set('id_servicio',this.items2['id_servicio']);    
  /*this.fecha="2017-22-10";
  this.items2.push(this.fecha);*/  
  
  
}

  mostrar_inicio_fase(fase)
  {
    let prompt = this.alertCtrl.create({
      title: 'Fase del Servicio',
      message: "Se ha Iniciado la fase"+fase,      
      buttons: [
        {
          text: 'Ok',
          handler: data => {
            
          }
        },       
      ]
    });
    prompt.present();
  }

origendestino(lat,lon,tipo_usuario,id_servicio,statusservice)
{ 
  
  this.navCtrl.push(OrigendestinoPage, {lat:lat,lon:lon,tipo_usuario,id_servicio,statusservice})
}
buscarregfase(fase)
{
    //se llama a la funcion que se encarga de indicar si existen registros para esta fase
    this.response_ = this.httpService.post2(this.variables.urlbase+"api/traerregmov",
    {
      id_servicio: this.localStorageService.get('id_servicio'),
      numero_fase: fase,
      iniciatermina:"inicia",
      latitud: 0,
      longitud:0
    });
    this.response_.map(res=>res)
    .subscribe(
      res=>{
         this.status=res.status;                   
         this.items3=res.json();            
      },
      err =>{
        this.status=err.status
      }    
    );        
}

armado()
{   
  //this.eventos_fase(2);   
  this.mostrar_inicio_fase("Armado");   
  this.buscarregfase(2);

  //se llama la pagina de armado-desarmado para operador  
  this.navCtrl.push(ArmadodesarmaoperadorPage,this.items2);
  
}
operacion()
{ 
  this.mostrar_inicio_fase("Operación");
  this.buscarregfase(3);
  this.navCtrl.push(ArmadodesarmaoperadorPage,this.items2)
}
desarmado()
{ 
  this.mostrar_inicio_fase("Desarmado");
  this.buscarregfase(4);
  this.navCtrl.push(ArmadodesarmaoperadorPage,this.items2)
}
encuesta()
{ 
  
  this.response_ = this.httpService.post(this.variables.urlbase + "api/cambiarfase",
  {

    fase_actual: 4,              
    id_servicio: this.localStorageService.get('id_servicio')
  });
  let myModal = this.modalCtrl.create(EncuestaPage); 
  myModal.present();  
  //this.navCtrl.push(EncuestaPage);
  

}

destinoorigen()
{
  //this.navCtrl.push(ArmadooperaciondesarmaPage,this.items2)
  //se llama el alert de la encuesta
  this.encuesta();
}
menuoperario()
{
  this.navCtrl.push(MenuoperarioPage)
}
menucliente()
{
  this.navCtrl.push(MenuclientePage)
}
historialservicios()
{
  this.navCtrl.push(HistorialserviciosPage);
}
verserviciosoperario()
{
  this.navCtrl.push(ServiciosoperarioPage)
}
salir()
{
  
    //se destruyen las vbles de localstorage
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('token_registration', "");
    this.localStorageService.set('token', "");
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('typeuser', "");    
    this.navCtrl.push(InicioPage)
 
}

}
