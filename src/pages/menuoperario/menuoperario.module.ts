import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuoperarioPage } from './menuoperario';

@NgModule({
  declarations: [
    MenuoperarioPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuoperarioPage),
  ],
})
export class MenuoperarioPageModule {}
