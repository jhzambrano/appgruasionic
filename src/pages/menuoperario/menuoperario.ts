import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{HistorialserviciosPage} from "../historialservicios/historialservicios";
import{ServiciosoperarioPage} from "../serviciosoperario/serviciosoperario";

import {LocalStorageService} from 'angular-2-local-storage';
import {InicioPage} from "../inicio/inicio"
import {MenuppaloperadorPage} from "../menuppaloperador/menuppaloperador"

import { PopoverController } from 'ionic-angular';
/**
 * Generated class for the MenuoperarioPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menuoperario',
  templateUrl: 'menuoperario.html',
})
export class MenuoperarioPage {

  constructor(private localStorageService: LocalStorageService,public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController) {
  }

  historialservicio(){
    this.navCtrl.push(ServiciosoperarioPage);
   
  }
  abrirmenu()
  {
    //this.navCtrl.push(SolosalirPage)
    let popover = this.popoverCtrl.create(MenuppaloperadorPage);
    popover.present({      
    });
  }
  salir(){
    //se destruyen las vbles de localstorage
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('token_registration', "");
    this.localStorageService.set('token', "");
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('typeuser', "");    
    this.navCtrl.push(InicioPage)
  }

}
