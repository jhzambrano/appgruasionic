import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiciosculminadosPage } from './serviciosculminados';

@NgModule({
  declarations: [
    ServiciosculminadosPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiciosculminadosPage),
  ],
})
export class ServiciosculminadosPageModule {}
