import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Http, RequestOptions, RequestMethod, Headers} from '@angular/http';
import {LocalStorageService} from 'angular-2-local-storage';
import{MenuclientePage} from "../menucliente/menucliente";
import {HttpService} from '../../services/http-service';
import {VARIALES} from '../../services/mock-vars';
import{TicketsPage} from "../tickets/tickets";
import{MenuservactivosPage} from "../menuservactivos/menuservactivos";

import { PopoverController } from 'ionic-angular';
/**
 * Generated class for the HistorialserviciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-serviciosculminados',
  templateUrl: 'serviciosculminados.html',
})
export class ServiciosculminadosPage {
  slideOneForm: FormGroup;
  items: any=[];  
  user_id: number;
  response_:any = [];
  response_d:any = [];
  response:any = [];
  responseData:any = [];
  status: any=[];
  constructor(private variables:VARIALES, private httpService:HttpService,private localStorageService: LocalStorageService,public navCtrl: NavController,private http:Http, public navParams: NavParams,public popoverCtrl: PopoverController) {
    this.setservicios();
  }
  abrirmenu(){
    
    let popover = this.popoverCtrl.create(MenuservactivosPage);
      popover.present({});
  }
  setservicios(){                           
              this.response_ = this.httpService.post2(this.variables.urlbase+"api/traerserviciosusuarioculminados",
              {
                user_id: this.localStorageService.get('id_user')
              });
              this.response_.map(res=>res)
              .subscribe(
                res=>{
                   this.status=res.status;                   
                   this.items=res.json();                        
                },
                err =>{
                  this.status=err.status
                }
              );
    
  }
  menucliente() {
    this.navCtrl.push(MenuclientePage);
  }
  dataticket(items:any[]){   
    
    this.navCtrl.push(TicketsPage,items);
}
  

}
