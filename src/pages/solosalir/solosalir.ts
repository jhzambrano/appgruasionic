import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LocalStorageService} from 'angular-2-local-storage';
import{InicioPage} from "../inicio/inicio"
/**
 * Generated class for the SolosalirPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-solosalir',
  templateUrl: 'solosalir.html',
})
export class SolosalirPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private localStorageService: LocalStorageService) {
  }
  salir(){
    this.localStorageService.clearAll();
    this.navCtrl.push(InicioPage)    
  }  

}
