import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolosalirPage } from './solosalir';

@NgModule({
  declarations: [
    SolosalirPage,
  ],
  imports: [
    IonicPageModule.forChild(SolosalirPage),
  ],
})
export class SolosalirPageModule {}
