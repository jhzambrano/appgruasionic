import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuservactivosPage } from './menuservactivos';

@NgModule({
  declarations: [
    MenuservactivosPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuservactivosPage),
  ],
})
export class MenuservactivosPageModule {}
