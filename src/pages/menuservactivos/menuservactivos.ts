import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';

import{HistorialserviciosPage} from "../historialservicios/historialservicios"
import{MenuclientePage} from "../menucliente/menucliente"
import{InicioPage} from "../inicio/inicio"
import {LocalStorageService} from 'angular-2-local-storage';
/**
 * Generated class for the OpcionesmenuclientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menuservactivos',
  templateUrl: 'menuservactivos.html',
})
export class MenuservactivosPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private view: ViewController,private localStorageService: LocalStorageService) {
  }
  salir(){
    this.localStorageService.clearAll();
    this.navCtrl.push(InicioPage)    
  }  
  menucliente()
  {
    this.navCtrl.push(MenuclientePage);
  }
  cerrar(){
    this.view.dismiss();
  }  

}
