import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OrigendestinoPage } from './origendestino';

@NgModule({
  declarations: [
    OrigendestinoPage,
  ],
  imports: [
    IonicPageModule.forChild(OrigendestinoPage),
  ],
})
export class OrigendestinoPageModule {}
