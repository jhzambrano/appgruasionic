import { Component , ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpService } from '../../services/http-service';
import { LocalStorageService } from 'angular-2-local-storage';
import { VARIALES } from '../../services/mock-vars';
import{HistorialserviciosPage} from "../historialservicios/historialservicios";
import{OpcionesmenuclientePage} from "../opcionesmenucliente/opcionesmenucliente";
import{MenumapaoperadorPage} from "../menumapaoperador/menumapaoperador";
import { PopoverController } from 'ionic-angular';
declare var google: any;

/**
 * Generated class for the OrigendestinoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-origendestino',
  templateUrl: 'origendestino.html',
})
export class OrigendestinoPage {

  @ViewChild('map') mapElement: ElementRef;
  map: any; 
  lat:any;
  lon:any;
   latc:any;
   tipo_usuario: any;
   id_servicio:any;
   statusservice:any;
  lonc:any;
  response_: any = [];
  status: any;
  items3: any=[];
  constructor(public geolocation: Geolocation,public platform: Platform, public navCtrl: NavController, public navParams: NavParams,private alertCtrl: AlertController,private httpService: HttpService,private localStorageService: LocalStorageService, private variables: VARIALES,public popoverCtrl: PopoverController) {
    this.lat = this.navParams.get('lat'); 
    this.lon = this.navParams.get('lon');
    this.tipo_usuario=this.navParams.get('tipo_usuario');
    this.id_servicio=this.navParams.get('id_servicio');
    this.statusservice=this.navParams.get('statusservice');
    //se llama al metodo que verifica si ese servicio en esa fase ya tiene registro. Para ello se envia el id_servicio,fase y se busca si el campo
    //startorend está en 1 o 2
    this.buscarregfase(this.statusservice);    
   platform.ready().then(() => { 
      this.loadMap();
     
    });
     this.addMarker2();
     setInterval(() => { this.addMarker(); }, 60000 );  
  }
  buscarregfase(statusservice)
  {
      let lt: any="";
      let lg: any="";      
      this.geolocation.getCurrentPosition().then((position)=>{
        lt=position.coords.latitude;
        lg=position.coords.longitude;        
        /******llamado funcion */
        //se llama a la funcion que se encarga de indicar si existen registros para esta fase      
      this.response_ = this.httpService.post2(this.variables.urlbase+"api/traerregmov",
      {
        id_servicio: this.localStorageService.get('id_servicio'),
        numero_fase: statusservice,
        iniciatermina:"inicia",
        latitud: lt,
        longitud: lg
      });
      this.response_.map(res=>res)
      .subscribe(
        res=>{
           this.status=res.status;                   
           this.items3=res.json();             
        },
        err =>{
          this.status=err.status
        }    
      );
        /**** */

        
      },(err)=>{            
      });

              
  }
   loadMap(){

  this.geolocation.getCurrentPosition().then((position) => {
 
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: true,  //quita zoom con scroll
        disableDefaultUI: true       
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
    }, (err) => {
    
    });
  }
  addMarker(){  
    this.geolocation.getCurrentPosition().then((position) => {
      this.latc = position.coords.latitude;
      this.lonc = position.coords.longitude;

    });


 var pos = {
             
              lng :  Number (  this.lonc),
              lat: Number (this.latc)
   };   
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: pos
  });
     
  let content = "";           
  this.addInfoWindow(marker, content);  

}

addMarker2(){
 var pos = {
             lng : Number(this.lon),
              lat: Number(this.lat)
   };   
   
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: pos
  });
     
  let content = "";           
  this.addInfoWindow(marker, content);  

  }

  addInfoWindow(marker, content){
  
    let infoWindow = new google.maps.InfoWindow({
      content: content
    });
  
    google.maps.event.addListener(marker, 'click', () => {
      infoWindow.open(this.map, marker);
    });
  
  }
  abrirmenu(tipo_usuario,id_servicio,statusservice)  
  { 
    if(tipo_usuario==1)
      this.navCtrl.push(OpcionesmenuclientePage)
    else
    {
      //se llama al menu del operario en el mapa
      //this.navCtrl.push(MenumapaoperadorPage)
      
      let popover = this.popoverCtrl.create(MenumapaoperadorPage,{id_servicio,statusservice});
      popover.present({      
      });
  
    }
  }
  finalizarFase(fase)
  {   
    let confirm = this.alertCtrl.create({
      title: '¿Seguro Desea Finalizar Esta Fase?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {

          }
        },
        {
          text: 'Si',
          handler: () => {
            //se llama al metodo que le cambia el status al servicio
            if(fase==4) //ya es la ultima... y se debe mostrar el alert que finaliza el servicio
            {
             // this.encuesta();
              let alert = this.alertCtrl.create({
                title: "Finalización del Servicio",
                subTitle: 'Su servicio ha finalizado con éxito',
                buttons: ['OK']
                });
                alert.present();
                //luego viene la vista con el rating. Se muestra otro alert con las caras y para agregar la descripcion
                /*******promp alert */
                
                /******* */

            }
            this.response_ = this.httpService.post(this.variables.urlbase + "api/cambiarfase",
            {
      
              fase_actual: fase,              
              id_servicio: this.localStorageService.get('id_servicio')
            });
            //se llama el metodo para que registre el movimiento
            
            //************************** */
            this.navCtrl.push(HistorialserviciosPage);

          }
        }
      ]
    });
    confirm.present();
  }

}
