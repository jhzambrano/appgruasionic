import { Component , ViewChild, ElementRef } from '@angular/core';
import {ModalController, IonicPage, NavController, NavParams, AlertController,Platform } from 'ionic-angular';
import {MenuclientePage} from "../menucliente/menucliente";
import {Http, RequestOptions, RequestMethod, Headers} from '@angular/http';
import {LocalStorageService} from 'angular-2-local-storage';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import 'rxjs/add/operator/map';
import { Geolocation } from '@ionic-native/geolocation';
import {VARIALES} from '../../services/mock-vars';
import {HttpService} from '../../services/http-service';

declare var google: any;

@IonicPage()
@Component({
  selector: 'page-cancelarservicio',
  templateUrl: 'cancelarservicio.html',
})


 
export class CancelarservicioPage {
 
@ViewChild('map') mapElement: ElementRef;
  map: any; 
  response_:any = [];
  response_d:any = [];
  slideOneForm: FormGroup; 
  servicetype:number;
  startdate:any;
  starttime:any;    
  items: any=[];  
  items_servicios: any=[];
  response:any = [];
  id_user:number;
  lat: string;
  long: string;
  observacion: string;
  id: number;
  constructor(private httpService:HttpService,private variables:VARIALES,public geolocation: Geolocation,public platform: Platform, public modalCtrl: ModalController, private localStorageService: LocalStorageService, private http:Http,public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController) {
   platform.ready().then(() => { 
      this.loadMap();
      });
   this.slideOneForm = formBuilder.group({
        id:['', Validators.compose([Validators.required ])],
        servicetype:['', Validators.compose([Validators.required ])],
        startdate: ['', Validators.compose([Validators.required ])],
        starttime: ['', Validators.compose([Validators.required])],        
        observacion: ['', Validators.compose([Validators.required])]        
    });
    this.serviciosparacliente(); 
    this.motivoscancelacion();
  }  
  ionViewDidLoad() {    
  }
  serviciosparacliente(){
    this.response_=this.httpService.post2(this.variables.urlbase+"api/serviciosparacliente",
    {
      iduser: this.localStorageService.get('id_user')
     
    } );
    this.response_.map(res=>res)
    .subscribe(
      res=>{
         
         this.items_servicios=res.json();  
      }
    );               
  }
  motivoscancelacion(){  
    let headers = new Headers(); 
    headers.append('Content-Type', 'application/json');
    headers.append('Authorization',  'Bearer '+this.localStorageService.get('token'));
    let options = new RequestOptions({ headers: headers });   
                this.http.get(this.variables.urlbase+"api/motivoscancelacion", 
                 options  
              ) 
              .subscribe(
                data => {  
                  
                  if(data.status == 200){
                     this.items = data.json();                    
                   }
                },			
                err => { err=err 
                 
                }
              );            
    
  }
  reprogramarservicio(){     
    //se capturan los valores de los campos            
    if(this.slideOneForm.valid){    
    //--------------------------------------------------------
      
        this.response_ = this.httpService.post(this.variables.urlbase+"api/reprogramarservicio",
        { id: this.id,          
          startdate: this.startdate,
          starttime: this.starttime,                               
          lat:this.localStorageService.get('lat'),
          long:this.localStorageService.get('long'),
          observacion: this.observacion  
        } );                   
        this.response_d = JSON.parse(this.response_);        
        this.response = this.response_d.rsp;                
        if(this.response_d.status == "200"){       
              let alert = this.alertCtrl.create({
                title: 'Reprogramación de Servicio',
                subTitle: 'Su Servicio de reprogramó correctamentes',
                buttons: ['OK']
              });
              alert.present()
              //se redirecciona al menu
              this.navCtrl.push(MenuclientePage);
             }
   
   
    }else{
      let alert = this.alertCtrl.create({
        title: 'Registro de Servicio',
        subTitle: 'Verifique que todos los datos sean ingresados',
        buttons: ['OK']
      });
      alert.present()
    }

  }
  menucliente(){
    this.navCtrl.push(MenuclientePage);
  }
 
 loadMap(){

  this.geolocation.getCurrentPosition().then((position) => {
 
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
 
      let mapOptions = {
        center: latLng,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
 
      this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
 
    }, (err) => {
      
    });
  }
 
 addMarker(){
 var pos = {
             lng : Number((this.map.getBounds().b.b + this.map.getBounds().b.f)/2),
              lat: Number((this.map.getBounds().f.b + this.map.getBounds().f.f)/2)
   };   
  let marker = new google.maps.Marker({
    map: this.map,
    animation: google.maps.Animation.DROP,
    position: pos
  });
     
  let content = "<h4>Information!</h4>";           
  this.addInfoWindow(marker, content);
  
  this.localStorageService.set('lat',pos.lat);
  this.localStorageService.set('long',pos.lng);
  

}
addInfoWindow(marker, content){
 
  let infoWindow = new google.maps.InfoWindow({
    content: content
  });
 
  google.maps.event.addListener(marker, 'click', () => {
    infoWindow.open(this.map, marker);
  });
 
}
 
}
