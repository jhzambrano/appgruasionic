import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CancelarservicioPage } from './cancelarservicio';

@NgModule({
  declarations: [
    CancelarservicioPage,
  ],
  imports: [
    IonicPageModule.forChild(CancelarservicioPage),
  ],
})
export class CancelarservicioPageModule {}
