import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{ServiciosoperarioPage}from '../serviciosoperario/serviciosoperario'
import{InicioPage}from '../inicio/inicio'
import { LocalStorageService } from 'angular-2-local-storage';
/**
 * Generated class for the MenuppaloperadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menuppaloperador',
  templateUrl: 'menuppaloperador.html',
})
export class MenuppaloperadorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private localStorageService: LocalStorageService) {
  }
  historialservicios()
  {
    this.navCtrl.push(ServiciosoperarioPage);
  }
  salir()
  {
    this.localStorageService.clearAll();
    this.navCtrl.push(InicioPage)    
  }  

}
