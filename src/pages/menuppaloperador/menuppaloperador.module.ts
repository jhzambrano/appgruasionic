import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuppaloperadorPage } from './menuppaloperador';

@NgModule({
  declarations: [
    MenuppaloperadorPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuppaloperadorPage),
  ],
})
export class MenuppaloperadorPageModule {}
