import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EndesarrolloPage } from './endesarrollo';

@NgModule({
  declarations: [
    EndesarrolloPage,
  ],
  imports: [
    IonicPageModule.forChild(EndesarrolloPage),
  ],
})
export class EndesarrolloPageModule {}
