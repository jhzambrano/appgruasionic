import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {MenuclientePage} from "../menucliente/menucliente";
/**
 * Generated class for the EndesarrolloPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-endesarrollo',
  templateUrl: 'endesarrollo.html',
})
export class EndesarrolloPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }  
  menucliente(){
    this.navCtrl.push(MenuclientePage);
  }

}
