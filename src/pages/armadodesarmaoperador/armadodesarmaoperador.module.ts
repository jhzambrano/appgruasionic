import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArmadodesarmaoperadorPage } from './armadodesarmaoperador';

@NgModule({
  declarations: [
    ArmadodesarmaoperadorPage,
  ],
  imports: [
    IonicPageModule.forChild(ArmadodesarmaoperadorPage),
  ],
})
export class ArmadodesarmaoperadorPageModule {}
