import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LocalStorageService} from 'angular-2-local-storage';
import{HistorialserviciosPage} from "../historialservicios/historialservicios";
import { HttpService } from '../../services/http-service';
import { VARIALES } from '../../services/mock-vars';

/**
 * Generated class for the EncuestaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-encuesta',
  templateUrl: 'encuesta.html',
})
export class EncuestaPage {
  slideOneForm: FormGroup; 
  nivelencuesta: number;
  relationship: number;
  descripcion: string;
  response_: any=[];
  calificacion: any;
  
  
  constructor(private localStorageService: LocalStorageService,public navCtrl: NavController, public navParams: NavParams, private view: ViewController,public formBuilder: FormBuilder,private variables: VARIALES,private httpService: HttpService) {
    this.slideOneForm = formBuilder.group({
      relationship: ['', Validators.compose([Validators.required ])],
      descripcion: ['', Validators.compose([Validators.required])],
      
  });
  }  
  posponer()
  {
    this.view.dismiss();
    /*this.response_ = this.httpService.post(this.variables.urlbase + "api/cambiarfase",
    {        
      id_servicio: this.localStorageService.get('id_servicio'),
      fase_actual:3
      
    });
    this.navCtrl.push(HistorialserviciosPage);*/
  }
  closeModal(){            
    this.view.dismiss();
    
    //se llama al metodo de laravel que actualiza el servicio
    this.response_ = this.httpService.post(this.variables.urlbase + "api/ultimafase",
    {        
      id_servicio: this.localStorageService.get('id_servicio'),
      calificacion:this.relationship,
      descripcion: this.descripcion
    });
   
    this.navCtrl.push(HistorialserviciosPage);
  }

}
