import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpcionesmenuclientePage } from './opcionesmenucliente';

@NgModule({
  declarations: [
    OpcionesmenuclientePage,
  ],
  imports: [
    IonicPageModule.forChild(OpcionesmenuclientePage),
  ],
})
export class OpcionesmenuclientePageModule {}
