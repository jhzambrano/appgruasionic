import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitarservicioPage } from './solicitarservicio';

@NgModule({
  declarations: [
    SolicitarservicioPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitarservicioPage),
  ],
})
export class SolicitarservicioPageModule {}
