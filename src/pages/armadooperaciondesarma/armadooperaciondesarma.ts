import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { HttpService } from '../../services/http-service';
import { VARIALES } from '../../services/mock-vars';
import { LocalStorageService } from 'angular-2-local-storage';
import{HistorialserviciosPage}from '../historialservicios/historialservicios';
import{MenuclientePage}from '../menucliente/menucliente';
import{OpcionesmenuclientePage}from '../opcionesmenucliente/opcionesmenucliente';

/**
 * Generated class for the ArmadooperaciondesarmaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-armadooperaciondesarma',
  templateUrl: 'armadooperaciondesarma.html',
})
export class ArmadooperaciondesarmaPage {
  slideOneForm: FormGroup;
  public photos: any;
  public base64Image: string;
  public evento: string;
  id_servicio: any;
  public status_servicio: any;
  response_: any = [];
  items2:any=[];
  items:any=[];
  items3:any=[];
  status: any;
  otra: any;
  esoperador: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder, private camera: Camera, private alertCtrl: AlertController, private variables: VARIALES, private httpService: HttpService, private localStorageService: LocalStorageService) {
    
    this.slideOneForm = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email, Validators.minLength(8), Validators.maxLength(120)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(4), Validators.maxLength(16)])]      
    }
    );
    this.items2=this.navParams.data;          
    this.eventos_fase(this.items2.statusservice);

  }
  eventos_fase(num_fase){        
  
    this.response_ = this.httpService.post2(this.variables.urlbase+"api/traereventosfase",
    {
      id_servicio: this.localStorageService.get('id_servicio'),
      numero_fase: num_fase
    });
    this.response_.map(res=>res)
    .subscribe(
      res=>{
         this.status=res.status;                   
         this.items3=res.json();                             
      },
      err =>{
        this.status=err.status
      }    
    );  
  }
  ngOnInit() {
    this.photos = [];
  }
  takePhoto() {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,

      /*saveToPhotoAlbum: true,
      allowEdit: true*/
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.base64Image = 'data:image/jpeg;base64,' + imageData;
      this.photos.push(this.base64Image); //agrega la imagen tomada con la cámara
      this.photos.reverse();
    }, (err) => {
      // Handle error
    });
  }
  deletePhoto(index, subida = 0) {
    if (subida == 1) //subió la foto
    {
      this.photos.splice(index, 1);
      //se muestra el alert que la foto está arriba
      let alert = this.alertCtrl.create({
        title: 'Carga de Imagenes',
        subTitle: 'La imagen se ha subido con Éxito..',
        buttons: ['OK']
      });
      alert.present();
    }
    else {
      let confirm = this.alertCtrl.create({
        title: '¿Seguro Desea Eliminar la Imagen?',
        message: '',
        buttons: [
          {
            text: 'No',
            handler: () => {

            }
          },
          {
            text: 'Si',
            handler: () => {
              this.photos.splice(index, 1);
            }
          }
        ]
      });
      confirm.present();
    }

  }
  upPhoto(index,statusservicio) {    
    this.response_ = this.httpService.post(this.variables.urlbase + "api/subirfoto",
      {

        foto: this.photos[index],
        evento: "empty",
        id_servicio: this.localStorageService.get('id_servicio'),
        status_servicio: statusservicio
      }

    );
    //despues de guardar elimina la foto
    this.deletePhoto(index, 1);

  }
  
  generarEvento(statusservicio) {        
      let prompt = this.alertCtrl.create({
        title: 'Registros de Eventos',
        message: "Agregue una descripción del evento presentado",
        inputs: [
          {
            name: 'nombreevento',
            placeholder: 'Nombre del evento'
          },
          {
            name: 'evento',
            placeholder: 'Desc Evento',            
          }
          
        ],       
        buttons: [
          {
            text: 'Cancelar',
            handler: data => {
              
            }
          },
          {
            text: 'Guardar',
            handler: data => {              
              //aqui se llama el metodo para guardar en la tabla                            
              this.response_ = this.httpService.post(this.variables.urlbase + "api/subirfoto",
              {
        
                foto: "empty",
                nombre_evento: data.nombreevento,
                evento: data.evento,
                id_servicio: this.localStorageService.get('id_servicio'),
                status_servicio: statusservicio
              });
              
            }
          }
        ]
      });
      prompt.present();
     
    
  }
  abrirmenu()
  {
    this.navCtrl.push(OpcionesmenuclientePage);
  }
  finalizarFase(fase)
  {    
    let confirm = this.alertCtrl.create({
      title: '¿Seguro Desea Finalizar Esta Fase?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {

          }
        },
        {
          text: 'Si',
          handler: () => {
            //se llama al metodo que le cambia el status al servicio
            if(fase==4) //ya es la ultima... y se debe mostrar el alert que finaliza el servicio
            {
             // this.encuesta();
              let alert = this.alertCtrl.create({
                title: "Finalización del Servicio",
                subTitle: 'Su servicio ha finalizado con éxito',
                buttons: ['OK']
                });
                alert.present();
                //luego viene la vista con el rating. Se muestra otro alert con las caras y para agregar la descripcion
                /*******promp alert */
                
                /******* */

            }
            this.response_ = this.httpService.post(this.variables.urlbase + "api/cambiarfase",
            {
      
              fase_actual: fase,              
              id_servicio: this.localStorageService.get('id_servicio')
            });
            this.navCtrl.push(HistorialserviciosPage);

          }
        }
      ]
    });
    confirm.present();
  }

  


}
