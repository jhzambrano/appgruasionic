import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArmadooperaciondesarmaPage } from './armadooperaciondesarma';

@NgModule({
  declarations: [
    ArmadooperaciondesarmaPage,
  ],
  imports: [
    IonicPageModule.forChild(ArmadooperaciondesarmaPage),
  ],
})
export class ArmadooperaciondesarmaPageModule {}
