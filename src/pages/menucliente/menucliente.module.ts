import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuclientePage } from './menucliente';

@NgModule({
  declarations: [
    MenuclientePage,
  ],
  imports: [
    IonicPageModule.forChild(MenuclientePage),
  ],
})
export class MenuclientePageModule {}
