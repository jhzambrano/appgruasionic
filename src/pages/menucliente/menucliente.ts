import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {SolicitarservicioPage} from "../solicitarservicio/solicitarservicio";
import {InicioPage} from "../inicio/inicio"
import{HistorialserviciosPage} from "../historialservicios/historialservicios";
import{CancelarservicioPage} from "../cancelarservicio/cancelarservicio";
import{ServiciosculminadosPage} from "../serviciosculminados/serviciosculminados";
import{SolosalirPage} from "../solosalir/solosalir";
import { PopoverController } from 'ionic-angular';
import {LocalStorageService} from 'angular-2-local-storage';

@IonicPage()
@Component({
  selector: 'page-menucliente',
  templateUrl: 'menucliente.html',
})
export class MenuclientePage {

  constructor(private localStorageService: LocalStorageService,public navCtrl: NavController, public navParams: NavParams,public popoverCtrl: PopoverController) {
  }
  abrirmenu()
  {
    //this.navCtrl.push(SolosalirPage)
    let popover = this.popoverCtrl.create(SolosalirPage);
    popover.present({      
    });
  }
  
  solicitarservicio()
  {
    //aqui se llama la vista para solicitar servicio
    this.navCtrl.push(SolicitarservicioPage);
  }
  serviciosporcalificar(){
    this.navCtrl.push(HistorialserviciosPage);
  }
  serviciosactivos()
  {
    this.navCtrl.push(HistorialserviciosPage);
  }
  historialservicio(){
    //this.navCtrl.push(HistorialserviciosPage);
    this.navCtrl.push(ServiciosculminadosPage);
  }
  cancelarservicio(){
    this.navCtrl.push(CancelarservicioPage)
  }
  salir(){
    //se destruyen las vbles de localstorage
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('token_registration', "");
    this.localStorageService.set('token', "");
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('typeuser', "");    
    this.navCtrl.push(InicioPage)
    this.localStorageService.clearAll();
  }

}
