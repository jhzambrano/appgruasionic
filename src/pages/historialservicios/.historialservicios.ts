import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Http, RequestOptions, RequestMethod, Headers} from '@angular/http';
import {LocalStorageService} from 'angular-2-local-storage';
import{MenuclientePage} from "../menucliente/menucliente";
import {HttpService} from '../../services/http-service';
import {VARIALES} from '../../services/mock-vars';
/**
 * Generated class for the HistorialserviciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-historialservicios',
  templateUrl: 'historialservicios.html',
})
export class HistorialserviciosPage {
  slideOneForm: FormGroup;
  items: any=[];  
  user_id: number;
  response_:any = [];
  response_d:any = [];
  response:any = [];
  responseData:any = [];
  constructor(private variables:VARIALES, private httpService:HttpService,private localStorageService: LocalStorageService,public navCtrl: NavController,private http:Http, public navParams: NavParams) {
    this.setservicios();
  }
  setservicios(){     

              this.response_ = this.httpService.post(this.variables.urlbase+"api/traerserviciosusuario",
              {
                user_id: this.localStorageService.get('id_user')
              } );
      
              this.response_d = JSON.parse(this.response_);
             this.response = this.response_d.rsp;
      
              if(this.response_d.status == "200"){
                this.items  = this.response;
              }
              
    
  }
  menucliente() {
    this.navCtrl.push(MenuclientePage);
  }

  

}
