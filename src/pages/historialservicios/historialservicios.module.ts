import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HistorialserviciosPage } from './historialservicios';

@NgModule({
  declarations: [
    HistorialserviciosPage,
  ],
  imports: [
    IonicPageModule.forChild(HistorialserviciosPage),
  ],
})
export class HistorialserviciosPageModule {}
