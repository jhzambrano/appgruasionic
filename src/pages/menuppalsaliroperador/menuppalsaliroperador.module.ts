import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenuppalsaliroperadorPage } from './menuppalsaliroperador';

@NgModule({
  declarations: [
    MenuppalsaliroperadorPage,
  ],
  imports: [
    IonicPageModule.forChild(MenuppalsaliroperadorPage),
  ],
})
export class MenuppalsaliroperadorPageModule {}
