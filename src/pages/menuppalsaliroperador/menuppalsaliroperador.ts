import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import{MenuoperarioPage} from "../menuoperario/menuoperario";
import{InicioPage} from "../inicio/inicio";
import {LocalStorageService} from 'angular-2-local-storage';
/**
 * Generated class for the MenuppalsaliroperadorPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menuppalsaliroperador',
  templateUrl: 'menuppalsaliroperador.html',
})
export class MenuppalsaliroperadorPage {

  constructor(public navCtrl: NavController, public navParams: NavParams,private localStorageService: LocalStorageService) {
  }
  menuppal()
  {
    this.navCtrl.push(MenuoperarioPage);
  }
  salir(){
    this.localStorageService.clearAll();
    this.navCtrl.push(InicioPage)  
  }  

}
