import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ViewController,AlertController } from 'ionic-angular';

import{HistorialserviciosPage} from "../historialservicios/historialservicios"
import{MenuclientePage} from "../menucliente/menucliente"
import{InicioPage} from "../inicio/inicio"
import{ServiciosoperarioPage} from "../serviciosoperario/serviciosoperario"
import{MenuoperarioPage} from "../menuoperario/menuoperario"
import { HttpService } from '../../services/http-service';
import {LocalStorageService} from 'angular-2-local-storage';
import { VARIALES } from '../../services/mock-vars';
import { Geolocation } from '@ionic-native/geolocation';
declare var google: any;
/**
 * Generated class for the OpcionesmenuclientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-menumapaoperador',
  templateUrl: 'menumapaoperador.html',
})
export class MenumapaoperadorPage {
  id_servicio:any;
  statusservice: any;  
  response_: any = [];
  longitud: any;
  status: any;
  items3: any=[];
  constructor(public geolocation: Geolocation,public navCtrl: NavController, public navParams: NavParams,private view: ViewController,private localStorageService: LocalStorageService,private alertCtrl: AlertController,private httpService: HttpService,private variables: VARIALES) {
    this.id_servicio=this.navParams.get('id_servicio');
    this.statusservice=this.navParams.get('statusservice');    
  }
  cambiarfase(fase)
  {
    let confirm = this.alertCtrl.create({
      title: '¿Seguro Desea Finalizar Esta Fase?',
      message: '',
      buttons: [
        {
          text: 'No',
          handler: () => {

          }
        },
        {
          text: 'Si',
          handler: () => {
            //se llama al metodo que le cambia el status al servicio
            if(fase==4) //ya es la ultima... y se debe mostrar el alert que finaliza el servicio
            {
             // this.encuesta();
              let alert = this.alertCtrl.create({
                title: "Finalización del Servicio",
                subTitle: 'Su servicio ha finalizado con éxito',
                buttons: ['OK']
                });
                alert.present();
                //luego viene la vista con el rating. Se muestra otro alert con las caras y para agregar la descripcion
                /*******promp alert */
                
                /******* */

            }
            this.response_ = this.httpService.post(this.variables.urlbase + "api/cambiarfase",
            {
      
              fase_actual: fase,              
              id_servicio: this.localStorageService.get('id_servicio')
            });            
            /*********** */
            let lt: any="";
            let lg: any="";  
            this.geolocation.getCurrentPosition().then((position) => {
              
                   let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
              
                   let mapOptions = {
                     center: latLng,
                     zoom: 15,
                     mapTypeId: google.maps.MapTypeId.ROADMAP,
                     scrollwheel: true,  //quita zoom con scroll
                     disableDefaultUI: true       
                   }
              
                //   this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
              
                 }, (err) => {
                   
                 });    
            this.geolocation.getCurrentPosition().then((position)=>{
              lt=position.coords.latitude;
              lg=position.coords.longitude;              
            /************ */

                    this.response_ = this.httpService.post2(this.variables.urlbase+"api/traerregmov",
                    {
                      id_servicio: this.localStorageService.get('id_servicio'),
                      numero_fase: fase,
                      iniciatermina:"termina",
                      latitud: lt,
                      longitud: lg
                    });
                    this.response_.map(res=>res)
                    .subscribe(
                      res=>{
                        this.status=res.status;                   
                        this.items3=res.json();                                                   
                      },
                      err =>{
                        this.status=err.status
                      }    
                    ); 
                },(err)=>{                  
            });      
            this.navCtrl.push(ServiciosoperarioPage);

          }
        }
      ]
    });
    confirm.present();
    
  }
  menuoperador()
  {
    this.navCtrl.push(MenuoperarioPage)
  }
  historialserviciosoperador()
  {
    this.navCtrl.push(ServiciosoperarioPage)
  }
  salir(){
    this.localStorageService.clearAll();
    this.navCtrl.push(InicioPage)    
  }
  serviciosactivos(){
    this.navCtrl.push(HistorialserviciosPage);
  }
  menucliente()
  {
    this.navCtrl.push(MenuclientePage);
  }
  cerrar(){
    this.view.dismiss();
  }  

}
