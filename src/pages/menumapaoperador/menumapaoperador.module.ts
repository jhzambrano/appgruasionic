import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MenumapaoperadorPage } from './menumapaoperador';

@NgModule({
  declarations: [
    MenumapaoperadorPage,
  ],
  imports: [
    IonicPageModule.forChild(MenumapaoperadorPage),
  ],
})
export class MenumapaoperadorPageModule {}
