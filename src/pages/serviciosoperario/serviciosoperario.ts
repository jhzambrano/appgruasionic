import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Http, RequestOptions, RequestMethod, Headers} from '@angular/http';
import {LocalStorageService} from 'angular-2-local-storage';
import{MenuclientePage} from "../menucliente/menucliente";
import{TicketsPage} from "../tickets/tickets";
import {HttpService} from '../../services/http-service';
import {VARIALES} from '../../services/mock-vars';
import{MenuoperarioPage}from "../menuoperario/menuoperario";
import{InicioPage}from "../inicio/inicio";
import{MenuppalsaliroperadorPage}from "../menuppalsaliroperador/menuppalsaliroperador";

import{TicektsoperadorPage}from "../ticektsoperador/ticektsoperador";
import { PopoverController } from 'ionic-angular';
/**
 * Generated class for the HistorialserviciosPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-serviciosoperario',
  templateUrl: 'serviciosoperario.html',
})
export class ServiciosoperarioPage {
  slideOneForm: FormGroup;
  items: any=[];  
  user_id: number;
  response_:any = [];
  response_d:any = [];
  response:any = [];
  responseData:any = [];
  status: any=[];
  constructor(private variables:VARIALES, private httpService:HttpService,private localStorageService: LocalStorageService,public navCtrl: NavController,private http:Http, public navParams: NavParams,public popoverCtrl: PopoverController) {
    this.setservicios();
  }
  setservicios(){                           
    
    this.response_ = this.httpService.post2(this.variables.urlbase+"api/traerserviciosparaoperario",
              {
                user_id: this.localStorageService.get('id_user')
              });
              this.response_.map(res=>res)
              .subscribe(
                res=>{
                   this.status=res.status;                   
                   this.items=res.json();  
                   
                },
                err =>{
                  this.status=err.status
                }
              );
    
  }
  dataticket(items:any[]){          
    //aqui se llama la vista con los detalles de ticket para operador    
    this.navCtrl.push(TicektsoperadorPage,items);
    //this.navCtrl.push(TicketsPage,items);
  }
  menuoperario() {
    this.navCtrl.push(MenuoperarioPage);
  }
abrirmenu()
{
  let popover = this.popoverCtrl.create(MenuppalsaliroperadorPage);
  popover.present({      
  });
}
salir()
{
  
    //se destruyen las vbles de localstorage
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('token_registration', "");
    this.localStorageService.set('token', "");
    this.localStorageService.set('id_user', "");
    this.localStorageService.set('typeuser', "");    
    this.navCtrl.push(InicioPage)
 
}
  

}
