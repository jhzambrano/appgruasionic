import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ServiciosoperarioPage } from './serviciosoperario';

@NgModule({
  declarations: [
    ServiciosoperarioPage,
  ],
  imports: [
    IonicPageModule.forChild(ServiciosoperarioPage),
  ],
})
export class ServiciosoperarioPageModule {}
