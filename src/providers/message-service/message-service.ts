import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

@Injectable()
export class MessageServiceProvider {

  private url: string="http://localhost/3000";
  constructor(private http: Http) {
    console.log('Hello MessageServiceProvider Provider');
  }
  getMessages(){
    return this.http.get(this.url)    
    .do(res=>console.log(res));
  }

}
