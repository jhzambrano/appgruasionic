import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
let apiUrl="http://localhost/PHP-Slim-Restful/api/"; //la url a la que va a acceder. Deberia ser donde se conecte a la BD y traiga infor

@Injectable()
export class AuthService {
  private url: string ="http://localhost/PHP-Slim-Restful/api";
  constructor(private http: Http) {
    console.log('Hello AuthServiceProvider Provider');
  }
  postData(credentials, type){
    return new Promise((resolve,reject)=>{
      let headers = new Headers();
      this.http.post(apiUrl+type,JSON.stringify(credentials),{headers: headers}).subscribe(res=>{
        resolve(res.json());
      }, (err)=>{
          reject(err);
      }) 
    })
  }

}
