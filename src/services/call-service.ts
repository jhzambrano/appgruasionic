import {Injectable} from "@angular/core"; 
import {Http, RequestOptions, Headers} from '@angular/http'; 

import 'rxjs/add/operator/map';

@Injectable()
export class CallService {
  private var: any = [] ; 
  private status: any ; 
  constructor(private http:Http) {
     
  }

  get2(url:string, options) {
      return  this.http.get(url,options)
  }

post(url:string,params) {
        let headers = new Headers();        
        headers.append('Content-Type', 'application/json');    
        let options = new RequestOptions({ headers: headers });

        this.http.post(url,JSON.stringify(params),options)
        .map(res => res)
        .subscribe(
          res => { 
           this.status =res.status;
           this.var = res.json(); 
          },
          err => { 
           this.status = err.status ;
          }		
      ); 
      return JSON.stringify({'status':this.status,'rsp':this.var} );  
  }

    
}