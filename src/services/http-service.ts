import {Injectable} from "@angular/core"; 
import {Http, RequestOptions, Headers} from '@angular/http'; 

import 'rxjs/add/operator/map';

@Injectable()
export class HttpService {
  private var: any = [] ; 
  private status: any ; 
  constructor(private http:Http) {
     
  }

  get(url:string, options) {

        this.http.get(url,options)
        .map(res => res)
        .subscribe(
          res => { 
           this.var = res.json(); 
          }			
      );
      return this.var;  
  }

post(url:string,params) {
        let headers = new Headers();        
        headers.append('Content-Type', 'application/json');    
        let options = new RequestOptions({ headers: headers });

        this.http.post(url,JSON.stringify(params),options)
        .map(res => res)
        .subscribe(
          res => { 
           this.status =res.status;
           this.var = res.json(); 
          },
          err => { 
           this.status = err.status ;
          }		
      ); 
      return JSON.stringify({'status':this.status,'rsp':this.var} );  
  }
  post2(url:string,params) {
    let headers = new Headers(); 
    headers.append('Content-Type', 'application/json'); 
    let options = new RequestOptions({ headers: headers });
    return this.http.post(url,JSON.stringify(params),options); 
    }

    
}